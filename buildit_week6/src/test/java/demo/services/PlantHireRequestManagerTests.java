package demo.services;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.hateoas.Link;

import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.models.PlantHireRequest;
import demo.models.repositories.PlantHireRequestRepository;
import demo.services.rentit.RentalService;

@RunWith(MockitoJUnitRunner.class)
public class PlantHireRequestManagerTests {
    @Mock
    RentalService catalog;
    @Mock
    PlantHireRequestRepository phrRepository;
    @InjectMocks
    PlantHireRequestManager phrManager;

    @Before
    public void setup() {
    	MockitoAnnotations.initMocks(this);
    }
    
	@Test
	public void testCreatePurchaseOrderWithConfirmation() throws Exception {
		// Initialize the instance of PlantHireRequest that is supposedly stored in the database
		PlantHireRequest phr_original = new PlantHireRequest();
		phr_original.setId(10001L);
		phr_original.setPlantRef("http://rentit.com/rest/plants/10001");
		phr_original.setStartDate(new LocalDate(2014, 10, 06).toDate());
		phr_original.setEndDate(new LocalDate(2014, 10, 10).toDate());
		
		PurchaseOrderResource po = new PurchaseOrderResource();
		po.add(new Link("http://rentit.com/rest/pos/10001"));
		po.setCost(1000f);
		
		PlantHireRequest phr_modified = new PlantHireRequest();
		phr_modified.setId(10001L);
		phr_modified.setPlantRef("http://rentit.com/rest/plants/10001");
		phr_modified.setStartDate(new LocalDate(2014, 10, 06).toDate());
		phr_modified.setEndDate(new LocalDate(2014, 10, 10).toDate());
		phr_modified.setPurchaseOrderRef("http://rentit.com/rest/pos/10001");
		phr_modified.setPrice(1000f);
	
		when(phrRepository.findOne(10001L)).thenReturn(phr_original);
		when(catalog.createPurchaseOrder(any(PlantResource.class), any(Date.class), any(Date.class))).thenReturn(po);

		phrManager.createPurchaseOrder(10001L);
		
		verify(phrRepository).save(phr_modified);
	}

	@Test(expected=PlantNotAvailableException.class)
	public void testCreatePurchaseOrderWithRejection() throws Exception {
		// Initialize the instance of PlantHireRequest that is supposedly stored in the database
		PlantHireRequest phr_original = new PlantHireRequest();
		phr_original.setId(10001L);
		phr_original.setPlantRef("http://rentit.com/rest/plants/10001");
		phr_original.setStartDate(new LocalDate(2014, 10, 06).toDate());
		phr_original.setEndDate(new LocalDate(2014, 10, 10).toDate());
		
		when(phrRepository.findOne(10001L)).thenReturn(phr_original);
		when(catalog.createPurchaseOrder(any(PlantResource.class), any(Date.class), any(Date.class)))
			.thenThrow(new PlantNotAvailableException());
		
		phrManager.createPurchaseOrder(10001L);
	}

}
