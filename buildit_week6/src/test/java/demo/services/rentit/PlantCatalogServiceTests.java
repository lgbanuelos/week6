package demo.services.rentit;

import static org.junit.Assert.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class PlantCatalogServiceTests {
	@Configuration
	static class TestConfiguration {
		@Bean
		public RestTemplate restTemplate() {
			RestTemplate _restTemplate = new RestTemplate();
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			_restTemplate.setMessageConverters(messageConverters);
			return _restTemplate;
		}
		@Bean
		public RentalService rentalService() {
			return new RentalService();
		}
	}

	@Autowired
	private RentalService rentItProxy;
	@Autowired
	private RestTemplate restTemplate;

	private MockRestServiceServer mockServer;

	@Before
	public void setup() {
		this.mockServer = MockRestServiceServer.createServer(this.restTemplate);
	}

	@Test
	public void testFindAvailablePlants() throws Exception {
		Resource responseBody = new ClassPathResource("AvailablePlantsV1.json", this.getClass());
		ObjectMapper mapper = new ObjectMapper();
		List<PlantResource> list = mapper.readValue(responseBody.getFile(),mapper.getTypeFactory().constructCollectionType(List.class, PlantResource.class));

		mockServer.expect(
				requestTo("http://rentit.com/rest/plants?name=Excavator&startDate=2014-10-06&endDate=2014-10-10")
		)
				.andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(responseBody, MediaType.APPLICATION_JSON));

		List<PlantResource> result = rentItProxy.findAvailablePlants("Excavator", new LocalDate(2014, 10, 6).toDate(), new LocalDate(2014, 10, 10).toDate());
		
		mockServer.verify();
		assertEquals(result, list);
	}

	@Test
	public void testCreatePurchaseOrder() throws Exception {
		ObjectMapper mapper = new ObjectMapper();

		PlantResource p = new PlantResource();
		PurchaseOrderResource po = new PurchaseOrderResource();
		po.setPlant(p);
		po.setStartDate(new LocalDate(2014, 10, 6).toDate());
		po.setEndDate(new LocalDate(2014, 10, 10).toDate());

		mockServer.expect(
				requestTo("http://rentit.com/rest/pos")
		)
				.andExpect(method(HttpMethod.POST))
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(content().string(mapper.writeValueAsString(po)))
				.andRespond(
						withCreatedEntity(new URI("http://rentit.com/rest/pos/345"))
						.body(mapper.writeValueAsString(po))
						.contentType(MediaType.APPLICATION_JSON)
						);

		PurchaseOrderResource result = rentItProxy.createPurchaseOrder(p, new LocalDate(2014, 10, 6).toDate(), new LocalDate(2014, 10, 10).toDate());

		mockServer.verify();
		assertEquals(po, result);
	}
}
