package demo.integration.rest;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import demo.models.PHRStatus;
import demo.models.PlantHireRequest;
import demo.services.PlantHireRequestManager;
import demo.services.PlantNotAvailableException;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PHRRestControllerUnitTests {
    private MockMvc mockMvc;
    
    @Mock
    PlantHireRequestManager phrManager;
    
    @InjectMocks
    PlantHireRequestRestController phrRestController;
     
    @Before
    public void setup() {
    	MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(phrRestController).build();
    }

	@Test
	public void testCreatePurchaseOrderWithConfirmation() throws Exception {		
		PlantHireRequest phr = new PlantHireRequest();
		phr.setId(10001L);
		phr.setPlantRef("http://rentit.com/rest/plants/10001");
		phr.setStartDate(new LocalDate(2014, 10, 06).toDate());
		phr.setEndDate(new LocalDate(2014, 10, 10).toDate());
		phr.setPurchaseOrderRef("http://rentit.com/rest/pos/10001");
		phr.setPrice(1000f);
		phr.setStatus(PHRStatus.PENDING);
	
		when(phrManager.createPurchaseOrder(10001L)).thenReturn(phr);

		String json = mockMvc.perform(post("/rest/phrs/{phr.id}/po", 10001))
			.andExpect(status().isCreated())
			// add something to check the resource "location"
			.andReturn().getResponse().getContentAsString();
		
		System.out.println(json);
	}
	
	@Test
	public void testCreatePurchaseOrderWithRejection() throws Exception {
		when(phrManager.createPurchaseOrder(10001L))
			.thenThrow(new PlantNotAvailableException());
		
		mockMvc.perform(post("/rest/phrs/{phr.id}/po", 10001))
			.andExpect(status().isConflict());
	}
}
