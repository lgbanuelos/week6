package demo.integration.dto;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import demo.integration.rest.PlantHireRequestRestController;
import demo.models.PlantHireRequest;

public class PlantHireRequestResourceAssembler extends ResourceAssemblerSupport<PlantHireRequest, PlantHireRequestResource> {

	public PlantHireRequestResourceAssembler() {
		super(PlantHireRequestRestController.class, PlantHireRequestResource.class);
	}

	@Override
	public PlantHireRequestResource toResource(PlantHireRequest phr) {
		PlantHireRequestResource res = createResourceWithId(phr.getId(), phr);
		res.setStartDate(phr.getStartDate());
		res.setEndDate(phr.getEndDate());
		res.setCost(phr.getPrice());
		
		if (phr.getPlantRef() != null) {
			PlantResource plantRes = new PlantResource();
			plantRes.add(new Link(phr.getPlantRef()));
			res.setPlant(plantRes);
		}
		if (phr.getPurchaseOrderRef() != null) {
			PurchaseOrderResource poRes = new PurchaseOrderResource();
			poRes.add(new Link(phr.getPurchaseOrderRef()));
			res.setPurchaseOrder(poRes);
		}

//		try {
//			switch (phr.getStatus()) {
//			case APPROVED:
//				res.add(linkTo(methodOn(PlantHireRequestRestController.class).createPurchaseOrder(phr.getId())).withRel("submitPurchaseOrder"));
//				break;
//			case PENDING:
//				res.add(linkTo(methodOn(PlantHireRequestRestController.class).approvePlantHireRequest(phr.getId())).withRel("approve"));
//				res.add(linkTo(methodOn(PlantHireRequestRestController.class).rejectPlantHireRequest(phr.getId())).withRel("reject"));
//				break;
//			case REJECTED:
//				break;
//			}
//		} catch(Exception e) {}
		
		return res;
	}

}
