package demo.integration.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.integration.dto.PlantHireRequestResourceAssembler;
import demo.services.PlantHireRequestManager;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@RestController
@RequestMapping("/rest/phrs")
public class PlantHireRequestRestController {
	@Autowired
	PlantHireRequestManager phrManager;
	PlantHireRequestResourceAssembler phrAssembler = new PlantHireRequestResourceAssembler();
}
