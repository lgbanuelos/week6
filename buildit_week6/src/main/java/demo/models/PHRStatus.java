package demo.models;

public enum PHRStatus {
	PENDING, APPROVED, REJECTED
}
