package demo.models.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.models.Plant;

@Repository
public interface PlantRepository extends JpaRepository<Plant, Long> {
	public List<Plant> findByNameLike(String name);
}
